package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/caarlos0/env"
	"gitlab.com/BIC_Dev/ark-log-parser/logparser"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/admin"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/chat"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/pve"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/pvp"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/tribe"
	"gitlab.com/BIC_Dev/log-parser-consumer/amazon"
	"gitlab.com/BIC_Dev/log-parser-consumer/nitrado"
	"gitlab.com/BIC_Dev/log-parser-consumer/utils/cache"
	"gitlab.com/BIC_Dev/log-parser-consumer/utils/logging"
	nitradoclient "gitlab.com/BIC_Dev/nitrado-api-client/client"
	"go.uber.org/zap"
)

type EnvVars struct {
	Environment              string `env:"ENVIRONMENT,required"`
	AWSRegion                string `env:"AWS_REGION,required"`
	AWSEndpoint              string `env:"AWS_ENDPOINT,required"`
	AWSS3LogLevel            uint   `env:"AWS_S3_LOG_LEVEL,required"`
	SQSQueueURL              string `env:"SQS_QUEUE_URL,required"`
	NitradoAPIClientHost     string `env:"NITRADO_API_CLIENT_HOST,required"`
	NitradoAPIClientBasepath string `env:"NITRADO_API_CLIENT_BASEPATH,required"`
	RedisHost                string `env:"REDIS_HOST,required"`
	RedisPort                int    `env:"REDIS_PORT,required"`
	RedisPoolSize            int    `env:"REDIS_POOL_SIZE,required"`
}

type QueueMessage struct {
	LogType      string `json:"log_type"`
	GuildID      string `json:"guild_id"`
	ServerID     string `json:"server_id"`
	ServerName   string `json:"server_name"`
	NitradoToken string `json:"nitrado_token"`
}

type MessageProcessor struct {
	sqsClient        sqs.SQS
	s3Client         s3.S3
	nitradoAPIClient nitradoclient.NitradoAPIClient
	cacheClient      cache.Cache
}

type S3LogDetails struct {
	GuildID    string `json:"guild_id"`
	ServerID   string `json:"server_id"`
	ServerName string `json:"server_name"`
	LogType    string `json:"log_type"`
}

type S3AdminLogs struct {
	Details S3LogDetails     `json:"details"`
	Logs    []admin.AdminLog `json:"logs"`
}

type S3ChatLogs struct {
	Details S3LogDetails   `json:"details"`
	Logs    []chat.ChatLog `json:"logs"`
}

type S3PVPLogs struct {
	Details S3LogDetails `json:"details"`
	Logs    []pvp.PVPLog `json:"logs"`
}

type S3PVELogs struct {
	Details S3LogDetails `json:"details"`
	Logs    []pve.PVELog `json:"logs"`
}

type S3TribeLogs struct {
	Details S3LogDetails     `json:"details"`
	Logs    []tribe.TribeLog `json:"logs"`
}

func main() {
	ctx := context.Background()
	envVars := EnvVars{}
	if err := env.Parse(&envVars); err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to parse environment variables"))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	local := envVars.Environment == "local"

	sess := amazon.NewSession(envVars.AWSRegion, envVars.AWSEndpoint, envVars.AWSS3LogLevel, local)
	sqsClient := amazon.NewSQSClient(sess)
	s3Client := amazon.NewS3Client(sess)
	nitradoAPIClient := nitrado.NewClient(envVars.NitradoAPIClientHost, envVars.NitradoAPIClientBasepath)
	cacheClient, ccErr := cache.NewClient(ctx, envVars.RedisHost, envVars.RedisPort, envVars.RedisPoolSize)
	if ccErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", ccErr), zap.String("error_message", ccErr.Message))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	var done chan bool = make(chan bool)
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		// We received an interrupt signal, shut down.
		done <- true
	}()

	mp := &MessageProcessor{*sqsClient, *s3Client, *nitradoAPIClient, *cacheClient}
	mp.pollQueue(ctx, envVars.SQSQueueURL, done) // Blocks

	// Give 60 seconds before shutting down
	time.Sleep(60 * time.Second)
	os.Exit(0)
}

func (mp *MessageProcessor) pollQueue(ctx context.Context, queueURL string, done chan bool) {
	concurrency := 3
	sync := createFullBufferedChannel(concurrency)
	for {
		select {
		case <-done:
			ctx = logging.AddValues(ctx, zap.String("message", "Shut down requested by AWS"))
			logger := logging.Logger(ctx)
			logger.Info("info_log")
			return
		default:
		}
		// Fetch some messages, hide it for 10 seconds.
		rmr := &sqs.ReceiveMessageInput{
			MaxNumberOfMessages: aws.Int64(3),
			VisibilityTimeout:   aws.Int64(60),
			WaitTimeSeconds:     aws.Int64(10),
			QueueUrl:            aws.String(queueURL),
			AttributeNames: []*string{
				aws.String(sqs.MessageSystemAttributeNameApproximateReceiveCount),
			},
		}
		rmre, rmErr := mp.sqsClient.ReceiveMessage(rmr)
		if rmErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", rmErr), zap.String("error_message", "Failed to retrieve message from SQS"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
		}

		// Sleep a little if we didn't find any messages in this poll.
		if len(rmre.Messages) < 1 {
			time.Sleep(time.Second)
			continue
		}

		// Iterate over the messages we received, and dispatch a processor for each.
		for i := range rmre.Messages {
			// Pull a worker from the queue
			<-sync

			tCtx := logging.AddValues(ctx, zap.String("queued_message", rmre.Messages[i].String()))
			logger := logging.Logger(tCtx)
			logger.Info("info_log")

			go func(i int) {
				mp.processMessage(ctx, rmre.Messages[i])

				_, dmErr := mp.sqsClient.DeleteMessage(&sqs.DeleteMessageInput{
					QueueUrl:      aws.String(queueURL),
					ReceiptHandle: rmre.Messages[i].ReceiptHandle,
				})

				if dmErr != nil {
					eCtx := logging.AddValues(ctx, zap.NamedError("error", dmErr), zap.String("error_message", "Failed to delete SQS message"))
					logger := logging.Logger(eCtx)
					logger.Error("error_log")
				}

				// Return worker to the queue to allow for more messages to be processed
				sync <- true
			}(i)
		}
	}
}

func (mp *MessageProcessor) processMessage(ctx context.Context, message *sqs.Message) {
	// fmt.Println(*message.Body)
	// dmr := &sqs.DeleteMessageRequest{
	// 	QueueURL:      aws.String(os.Getenv("SQS_QUEUE")),
	// 	ReceiptHandle: message.ReceiptHandle,
	// }
	// err := mp.client.DeleteMessage(dmr)
	// fmt.Println(err)

	var qm QueueMessage
	err := json.Unmarshal([]byte(*message.Body), &qm)
	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to umarshal message"))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	ctx = logging.AddValues(ctx, zap.String("guild_id", qm.GuildID), zap.String("server_id", qm.ServerID), zap.String("log_type", qm.LogType))

	gs, gsErr := nitrado.GetGameserver(&mp.nitradoAPIClient, qm.ServerID, qm.NitradoToken)
	if gsErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", gsErr), zap.String("error_message", "Failed to get gameserver details"))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	if gs.Status != "started" && gs.Status != "restarting" && gs.Status != "stopped" {
		ctx = logging.AddValues(ctx, zap.NamedError("error", fmt.Errorf("invalid state: %s", gs.Status)), zap.String("error_message", "Server is not in correct state"))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	username := gs.Username
	game := gs.Game
	oldLogPath := fmt.Sprintf("/games/%s/noftp/%s/ShooterGame/Saved/Logs/ShooterGame_Last.log", username, game)
	newLogPath := fmt.Sprintf("/games/%s/noftp/%s/ShooterGame/Saved/Logs/ShooterGame.log", username, game)

	s3LogDetails := S3LogDetails{
		GuildID:    qm.GuildID,
		ServerID:   qm.ServerID,
		ServerName: qm.ServerName,
	}

	var s3AdminLogs S3AdminLogs = S3AdminLogs{
		Details: s3LogDetails,
	}
	var s3ChatLogs S3ChatLogs = S3ChatLogs{
		Details: s3LogDetails,
	}
	var s3PVELogs S3PVELogs = S3PVELogs{
		Details: s3LogDetails,
	}
	var s3PVPLogs S3PVPLogs = S3PVPLogs{
		Details: s3LogDetails,
	}
	var s3TribeLogs S3TribeLogs = S3TribeLogs{
		Details: s3LogDetails,
	}

	s3AdminLogs.Details.LogType = "admin"
	s3ChatLogs.Details.LogType = "chat"
	s3PVELogs.Details.LogType = "pve"
	s3PVPLogs.Details.LogType = "pvp"
	s3TribeLogs.Details.LogType = "tribe"

	var latestTimestamp int64
	var latestModified int32
	var latestSize int32
	needToParseCurrentLog := false
	needToParsePreviousLog := false

	var logDetails cache.LogDetails
	cErr := mp.cacheClient.GetStruct(ctx, cache.GenerateKey("LOG_PARSE", fmt.Sprintf("%s:%s", qm.GuildID, qm.ServerID)), &logDetails)
	if cErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", cErr), zap.String("error_message", cErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	if logDetails.LastTimestamp == 0 {
		needToParseCurrentLog = true
		needToParsePreviousLog = true
	} else {
		currentStat, csErr := nitrado.GetStats(&mp.nitradoAPIClient, qm.ServerID, newLogPath, qm.NitradoToken)
		if csErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", csErr), zap.String("error_message", "Failed to get current log stats"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		if currentStat.ModifiedAt <= logDetails.LastModified && currentStat.Size <= logDetails.LastSize {
			ctx = logging.AddValues(ctx, zap.String("message", "no updated files to parse"))
			logger := logging.Logger(ctx)
			logger.Info("info_log")
			return
		} else {
			needToParseCurrentLog = true
		}

		latestModified = currentStat.ModifiedAt
		latestSize = currentStat.Size

		prevStat, csErr := nitrado.GetStats(&mp.nitradoAPIClient, qm.ServerID, oldLogPath, qm.NitradoToken)
		if csErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", csErr), zap.String("error_message", "Failed to get previous log stats"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		if prevStat.ModifiedAt > logDetails.LastModified {
			needToParsePreviousLog = true
		}
	}

	if needToParsePreviousLog {
		previousLogDownloadInfo, pldtErr := nitrado.GetDownloadURL(&mp.nitradoAPIClient, qm.ServerID, oldLogPath, qm.NitradoToken)
		if pldtErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", pldtErr), zap.String("error_message", "Failed to get previous log entry download url"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		previousLogFile, plfErr := nitrado.DownloadFile(previousLogDownloadInfo)
		if plfErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", plfErr), zap.String("error_message", "Failed to download previous log file"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		previousLogs, plErr := logparser.ParseLogs(previousLogFile)
		if plErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", plErr), zap.String("error_message", "Failed to parse previous log file"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		previousLogFile.Close()

		s3AdminLogs.Logs = append(s3AdminLogs.Logs, removeOldAdminLogEvents(logDetails.LastTimestamp, previousLogs.Admin)...)

		s3ChatLogs.Logs = append(s3ChatLogs.Logs, removeOldChatLogEvents(logDetails.LastTimestamp, previousLogs.Chat)...)

		s3PVELogs.Logs = append(s3PVELogs.Logs, removeOldPVELogEvents(logDetails.LastTimestamp, previousLogs.PVE)...)

		s3PVPLogs.Logs = append(s3PVPLogs.Logs, removeOldPVPLogEvents(logDetails.LastTimestamp, previousLogs.PVP)...)

		s3TribeLogs.Logs = append(s3TribeLogs.Logs, removeOldTribeLogEvents(logDetails.LastTimestamp, previousLogs.Tribe)...)

		latestTimestamp = previousLogs.Details.LastTimestamp

		previousLogs = nil
	}

	if needToParseCurrentLog {
		currentLogDownloadInfo, pldtErr := nitrado.GetDownloadURL(&mp.nitradoAPIClient, qm.ServerID, newLogPath, qm.NitradoToken)
		if pldtErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", pldtErr), zap.String("error_message", "Failed to get current log entry download url"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		currentLogFile, plfErr := nitrado.DownloadFile(currentLogDownloadInfo)
		if plfErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", plfErr), zap.String("error_message", "Failed to download current log file"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		currentLogs, plErr := logparser.ParseLogs(currentLogFile)
		if plErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", plErr), zap.String("error_message", "Failed to parse current log file"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}
		currentLogFile.Close()

		s3AdminLogs.Logs = append(s3AdminLogs.Logs, removeOldAdminLogEvents(logDetails.LastTimestamp, currentLogs.Admin)...)

		s3ChatLogs.Logs = append(s3ChatLogs.Logs, removeOldChatLogEvents(logDetails.LastTimestamp, currentLogs.Chat)...)

		s3PVELogs.Logs = append(s3PVELogs.Logs, removeOldPVELogEvents(logDetails.LastTimestamp, currentLogs.PVE)...)

		s3PVPLogs.Logs = append(s3PVPLogs.Logs, removeOldPVPLogEvents(logDetails.LastTimestamp, currentLogs.PVP)...)

		s3TribeLogs.Logs = append(s3TribeLogs.Logs, removeOldTribeLogEvents(logDetails.LastTimestamp, currentLogs.Tribe)...)

		if currentLogs.Details.LastTimestamp > latestTimestamp {
			latestTimestamp = currentLogs.Details.LastTimestamp
		}

		currentLogs = nil
	}

	mp.cacheClient.SetStruct(ctx, cache.GenerateKey("LOG_PARSE", fmt.Sprintf("%s:%s", qm.GuildID, qm.ServerID)), &cache.LogDetails{
		LastTimestamp: latestTimestamp,
		LastModified:  latestModified,
		LastSize:      latestSize,
	}, "604800") // 7 days

	currentTime := time.Now().Unix()

	if len(s3AdminLogs.Logs) > 0 {
		var buf bytes.Buffer
		err := json.NewEncoder(&buf).Encode(s3AdminLogs)
		if err != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to JSON encode admin logs"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		aErr := amazon.UploadJSONToS3(&mp.s3Client, &buf, "server-logs", fmt.Sprintf("/%s/%s/%s/admin/%d.json", qm.LogType, qm.GuildID, qm.ServerID, currentTime))
		if aErr != nil {
			eCtx := logging.AddValues(ctx, zap.NamedError("error", aErr), zap.String("error_message", "Failed to upload admin log json to S3"))
			logger := logging.Logger(eCtx)
			logger.Error("error_log")
		}
	}

	if len(s3ChatLogs.Logs) > 0 {
		var buf bytes.Buffer
		err := json.NewEncoder(&buf).Encode(s3ChatLogs)
		if err != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to JSON encode chat logs"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		aErr := amazon.UploadJSONToS3(&mp.s3Client, &buf, "server-logs", fmt.Sprintf("/%s/%s/%s/chat/%d.json", qm.LogType, qm.GuildID, qm.ServerID, currentTime))
		if aErr != nil {
			eCtx := logging.AddValues(ctx, zap.NamedError("error", aErr), zap.String("error_message", "Failed to upload chat log json to S3"))
			logger := logging.Logger(eCtx)
			logger.Error("error_log")
		}
	}

	if len(s3PVPLogs.Logs) > 0 {
		var buf bytes.Buffer
		err := json.NewEncoder(&buf).Encode(s3PVPLogs)
		if err != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to JSON encode pvp logs"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		aErr := amazon.UploadJSONToS3(&mp.s3Client, &buf, "server-logs", fmt.Sprintf("/%s/%s/%s/pvp/%d.json", qm.LogType, qm.GuildID, qm.ServerID, currentTime))
		if aErr != nil {
			eCtx := logging.AddValues(ctx, zap.NamedError("error", aErr), zap.String("error_message", "Failed to upload pvp log json to S3"))
			logger := logging.Logger(eCtx)
			logger.Error("error_log")
		}
	}

	if len(s3PVELogs.Logs) > 0 {
		var buf bytes.Buffer
		err := json.NewEncoder(&buf).Encode(s3PVELogs)
		if err != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to JSON encode pve logs"))
			logger := logging.Logger(ctx)
			logger.Error("error_log")
			return
		}

		aErr := amazon.UploadJSONToS3(&mp.s3Client, &buf, "server-logs", fmt.Sprintf("/%s/%s/%s/pve/%d.json", qm.LogType, qm.GuildID, qm.ServerID, currentTime))
		if aErr != nil {
			eCtx := logging.AddValues(ctx, zap.NamedError("error", aErr), zap.String("error_message", "Failed to upload pve log json to S3"))
			logger := logging.Logger(eCtx)
			logger.Error("error_log")
		}
	}

	if len(s3TribeLogs.Logs) > 0 {
		separatedTribeLogs := make(map[string]S3TribeLogs)
		for _, log := range s3TribeLogs.Logs {
			if _, ok := separatedTribeLogs[log.KilledTribeID]; !ok {
				separatedTribeLogs[log.KilledTribeID] = S3TribeLogs{
					Details: s3TribeLogs.Details,
					Logs:    []tribe.TribeLog{log},
				}
			} else {
				appendLogs := separatedTribeLogs[log.KilledTribeID]
				appendLogs.Logs = append(appendLogs.Logs, log)
				separatedTribeLogs[log.KilledTribeID] = appendLogs
			}
		}

		for tribeID, logs := range separatedTribeLogs {
			var buf bytes.Buffer
			err := json.NewEncoder(&buf).Encode(logs)
			if err != nil {
				ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to JSON encode tribe logs"))
				logger := logging.Logger(ctx)
				logger.Error("error_log")
				return
			}

			aErr := amazon.UploadJSONToS3(&mp.s3Client, &buf, "server-logs", fmt.Sprintf("/%s/%s/%s/tribe/%s/%d.json", qm.LogType, qm.GuildID, qm.ServerID, tribeID, currentTime))
			if aErr != nil {
				eCtx := logging.AddValues(ctx, zap.NamedError("error", aErr), zap.String("error_message", "Failed to upload tribe log json to S3"))
				logger := logging.Logger(eCtx)
				logger.Error("error_log")
			}
		}
	}

}

func removeOldAdminLogEvents(timestamp int64, logs []admin.AdminLog) []admin.AdminLog {
	var output []admin.AdminLog
	for _, event := range logs {
		if event.Time <= timestamp {
			continue
		}

		output = append(output, event)
	}

	return output
}

func removeOldChatLogEvents(timestamp int64, logs []chat.ChatLog) []chat.ChatLog {
	var output []chat.ChatLog
	for _, event := range logs {
		if event.Time <= timestamp {
			continue
		}

		output = append(output, event)
	}

	return output
}

func removeOldPVPLogEvents(timestamp int64, logs []pvp.PVPLog) []pvp.PVPLog {
	var output []pvp.PVPLog
	for _, event := range logs {
		if event.Time <= timestamp {
			continue
		}

		output = append(output, event)
	}

	return output
}

func removeOldPVELogEvents(timestamp int64, logs []pve.PVELog) []pve.PVELog {
	var output []pve.PVELog
	for _, event := range logs {
		if event.Time <= timestamp {
			continue
		}

		output = append(output, event)
	}

	return output
}

func removeOldTribeLogEvents(timestamp int64, logs []tribe.TribeLog) []tribe.TribeLog {
	var output []tribe.TribeLog
	for _, event := range logs {
		if event.Time <= timestamp {
			continue
		}

		output = append(output, event)
	}

	return output
}

func createFullBufferedChannel(capacity int) chan bool {
	sync := make(chan bool, capacity)

	for i := 0; i < capacity; i++ {
		sync <- true
	}
	return sync
}
