package cache

type LogDetails struct {
	LastTimestamp int64 `json:"last_timestamp"`
	LastModified  int32 `json:"last_modified"`
	LastSize      int32 `json:"last_size"`
}
