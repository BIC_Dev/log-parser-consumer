package nitrado

import (
	"context"
	"errors"
	"time"

	nitradoclient "gitlab.com/BIC_Dev/nitrado-api-client/client"
	"gitlab.com/BIC_Dev/nitrado-api-client/client/gameserver"
)

func GetStats(client *nitradoclient.NitradoAPIClient, serverID string, logPath string, nitradoToken string) (gameserver.FilesStatsOKBodyDataEntriesAnon, error) {
	params := &gameserver.FilesStatsParams{
		Files0:    logPath,
		ServiceID: serverID,
	}
	params.SetTimeout(10 * time.Second)
	params.SetContext(context.Background())

	stats, err := client.Gameserver.FilesStats(params, NitradoBearerToken(nitradoToken))
	if err != nil {
		return gameserver.FilesStatsOKBodyDataEntriesAnon{}, err
	}

	if stats.Payload == nil {
		return gameserver.FilesStatsOKBodyDataEntriesAnon{}, errors.New("no stats payload")
	}

	if stats.Payload.Data == nil {
		return gameserver.FilesStatsOKBodyDataEntriesAnon{}, errors.New("no stats payload data")
	}

	if stats.Payload.Data.Entries == nil {
		return gameserver.FilesStatsOKBodyDataEntriesAnon{}, errors.New("no stats payload data entries")
	}

	if _, ok := stats.Payload.Data.Entries[logPath]; !ok {
		return gameserver.FilesStatsOKBodyDataEntriesAnon{}, errors.New("no stats payload data entries for file")
	}

	return stats.Payload.Data.Entries[logPath], nil
}
