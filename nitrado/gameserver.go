package nitrado

import (
	"context"
	"errors"
	"time"

	nitradoclient "gitlab.com/BIC_Dev/nitrado-api-client/client"
	"gitlab.com/BIC_Dev/nitrado-api-client/client/gameserver"
)

func GetGameserver(client *nitradoclient.NitradoAPIClient, serverID string, nitradoToken string) (*gameserver.GetGameserverOKBodyDataGameserver, error) {
	params := &gameserver.GetGameserverParams{
		ServiceID: serverID,
	}
	params.SetTimeout(15 * time.Second)
	params.SetContext(context.Background())

	stats, err := client.Gameserver.GetGameserver(params, NitradoBearerToken(nitradoToken))
	if err != nil {
		return nil, err
	}

	if stats.Payload == nil {
		return nil, errors.New("no gameserver payload")
	}

	if stats.Payload.Data == nil {
		return nil, errors.New("no gameserver payload data")
	}

	if stats.Payload.Data.Gameserver == nil {
		return nil, errors.New("no gameserver payload data gameserver")
	}

	return stats.Payload.Data.Gameserver, nil
}
