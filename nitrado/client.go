package nitrado

import (
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	nitradoclient "gitlab.com/BIC_Dev/nitrado-api-client/client"
)

func NewClient(host string, basepath string) *nitradoclient.NitradoAPIClient {
	transport := nitradoclient.TransportConfig{
		Host:     host,
		BasePath: basepath,
		Schemes:  nil,
	}

	return nitradoclient.NewHTTPClientWithConfig(strfmt.Default, &transport)
}

// NitradoBearerToken func
func NitradoBearerToken(token string) runtime.ClientAuthInfoWriter {
	return runtime.ClientAuthInfoWriterFunc(func(r runtime.ClientRequest, _ strfmt.Registry) error {
		return r.SetHeaderParam("Authorization", "Bearer "+token)
	})
}
