package nitrado

import (
	"context"
	"errors"
	"io"
	"net/http"
	"time"

	nitradoclient "gitlab.com/BIC_Dev/nitrado-api-client/client"
	"gitlab.com/BIC_Dev/nitrado-api-client/client/gameserver"
)

func GetDownloadURL(client *nitradoclient.NitradoAPIClient, serverID string, logPath string, nitradoToken string) (*gameserver.DownloadFileOKBodyDataToken, error) {
	params := &gameserver.DownloadFileParams{
		File:      logPath,
		ServiceID: serverID,
	}
	params.SetTimeout(10 * time.Second)
	params.SetContext(context.Background())

	download, err := client.Gameserver.DownloadFile(params, NitradoBearerToken(nitradoToken))
	if err != nil {
		return nil, err
	}

	if download.Payload == nil {
		return nil, errors.New("no download payload")
	}

	if download.Payload.Data == nil {
		return nil, errors.New("no download payload data")
	}

	if download.Payload.Data.Token == nil {
		return nil, errors.New("no download payload data token")
	}

	return download.Payload.Data.Token, nil
}

func DownloadFile(fileInfo *gameserver.DownloadFileOKBodyDataToken) (io.ReadCloser, error) {
	client := &http.Client{
		Timeout: 60 * time.Second,
	}
	req, err := http.NewRequest(http.MethodGet, fileInfo.URL, nil)
	if err != nil {
		return nil, err
	}

	resp, doErr := client.Do(req)
	if doErr != nil {
		return nil, doErr
	}

	return resp.Body, nil
}
