export ENVIRONMENT=$(echo ${ENV_VARS} | jq -r '.ENVIRONMENT')
export AWS_REGION=$(echo ${ENV_VARS} | jq -r '.AWS_REGION')
export AWS_ENDPOINT=$(echo ${ENV_VARS} | jq -r '.AWS_ENDPOINT')
export AWS_S3_LOG_LEVEL=$(echo ${ENV_VARS} | jq -r '.AWS_S3_LOG_LEVEL')
export SQS_QUEUE_URL=$(echo ${ENV_VARS} | jq -r '.SQS_QUEUE_URL')
export NITRADO_API_CLIENT_HOST=$(echo ${ENV_VARS} | jq -r '.NITRADO_API_CLIENT_HOST')
export NITRADO_API_CLIENT_BASEPATH=$(echo ${ENV_VARS} | jq -r '.NITRADO_API_CLIENT_BASEPATH')
export REDIS_HOST=$(echo ${ENV_VARS} | jq -r '.REDIS_HOST')
export REDIS_PORT=$(echo ${ENV_VARS} | jq -r '.REDIS_PORT')
export REDIS_POOL_SIZE=$(echo ${ENV_VARS} | jq -r '.REDIS_POOL_SIZE')

export AWS_ACCESS_KEY_ID=$(echo ${ENV_VARS} | jq -r '.AWS_ACCESS_KEY_ID')
export AWS_SECRET_ACCESS_KEY=$(echo ${ENV_VARS} | jq -r '.AWS_SECRET_ACCESS_KEY')

/main