package main

import (
	"encoding/json"
	"io"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"gitlab.com/BIC_Dev/log-parser-consumer/amazon"
)

// func BenchmarkProcessMessage(t *testing.B) {
// 	ctx := context.Background()
// 	nitradoAPIClient := nitrado.NewClient("api.nitrado.net", "/")
// 	cacheClient, ccErr := cache.NewClient(ctx, "localhost", 6379, 1)
// 	sess := amazon.NewSession("us-west-2", "http://localhost.localstack.cloud:4566", 0)
// 	sqsClient := amazon.NewSQSClient(sess)
// 	s3Client := amazon.NewS3Client(sess)

// 	if ccErr != nil {
// 		t.Errorf("%s: %s", ccErr.Message, ccErr)
// 	}

// 	mp := &MessageProcessor{
// 		nitradoAPIClient: *nitradoAPIClient,
// 		cacheClient:      *cacheClient,
// 		sqsClient:        *sqsClient,
// 		s3Client:         *s3Client,
// 	}

// 	input := &QueueMessage{
// 		LogType:      "ark1",
// 		GuildID:      os.Getenv("TEST_GUILD_ID"),
// 		ServerID:     os.Getenv("TEST_SERVER_ID"),
// 		ServerName:   "Test Server",
// 		NitradoToken: os.Getenv("TEST_NITRADO_TOKEN"),
// 	}

// 	inputJson, err := json.Marshal(input)
// 	if err != nil {
// 		t.Errorf("Failed to marshal queue message: %s", err)
// 	}
// 	inputMD5 := md5.Sum(inputJson)

// 	t.ResetTimer()

// 	mp.processMessage(ctx, &sqs.Message{
// 		MD5OfBody: aws.String(string(hex.EncodeToString(inputMD5[:]))),
// 		Body:      aws.String(string(inputJson)),
// 	})
// }

// func BenchmarkReadQueue(t *testing.B) {
// 	ctx := context.Background()
// 	nitradoAPIClient := nitrado.NewClient("api.nitrado.net", "/")
// 	cacheClient, ccErr := cache.NewClient(ctx, "localhost", 6379, 1)
// 	sess := amazon.NewSession("us-east-1", "http://localhost.localstack.cloud:4566", 0)
// 	sqsClient := amazon.NewSQSClient(sess)
// 	s3Client := amazon.NewS3Client(sess)

// 	if ccErr != nil {
// 		t.Errorf("%s: %s", ccErr.Message, ccErr)
// 	}

// 	mp := &MessageProcessor{
// 		nitradoAPIClient: *nitradoAPIClient,
// 		cacheClient:      *cacheClient,
// 		sqsClient:        *sqsClient,
// 		s3Client:         *s3Client,
// 	}

// 	// _, smErr := sqsClient.SendMessage(&sqs.SendMessageInput{
// 	// 	MessageBody: aws.String("{\"log_type\":\"ark1\",\"guild_id\":\"\",\"server_id\":\"\",\"server_name\":\"Test Server\",\"nitrado_token\":\"\"}"),
// 	// 	QueueUrl:    aws.String("http://localhost.localstack.cloud:4566/000000000000/server-logs"),
// 	// })

// 	// if smErr != nil {
// 	// 	t.Fatalf("Failed to send message to SQS: %s", smErr)
// 	// }

// 	t.ResetTimer()

// 	var done chan bool = make(chan bool)
// 	go func() {
// 		time.Sleep(10 * time.Second)
// 		done <- true
// 	}()

// 	mp.pollQueue(ctx, "http://localhost.localstack.cloud:4566/000000000000/server-logs", done) // Blocks
// }

type TestMessage struct {
	LogType      string `json:"log_type"`
	GuildID      string `json:"guild_id"`
	ServerID     string `json:"server_id"`
	ServerName   string `json:"server_name"`
	NitradoToken string `json:"nitrado_token"`
}

// func Benchmark20Servers(t *testing.B) {
// 	ctx := context.Background()
// 	nitradoAPIClient := nitrado.NewClient("api.nitrado.net", "/")
// 	cacheClient, ccErr := cache.NewClient(ctx, "localhost", 6379, 1)
// 	sess := amazon.NewSession("us-east-1", "http://localhost.localstack.cloud:4566", 0, true)
// 	sqsClient := amazon.NewSQSClient(sess)
// 	s3Client := amazon.NewS3Client(sess)

// 	if ccErr != nil {
// 		t.Errorf("%s: %s", ccErr.Message, ccErr)
// 	}

// 	mp := &MessageProcessor{
// 		nitradoAPIClient: *nitradoAPIClient,
// 		cacheClient:      *cacheClient,
// 		sqsClient:        *sqsClient,
// 		s3Client:         *s3Client,
// 	}

// 	f, fErr := os.Open("./testdata/20-test-servers.json")
// 	if fErr != nil {
// 		t.Fatal(fErr)
// 	}

// 	var tests []TestMessage
// 	byteValue, _ := ioutil.ReadAll(f)
// 	jsErr := json.Unmarshal(byteValue, &tests)
// 	if jsErr != nil {
// 		t.Fatal(jsErr)
// 	}

// 	for _, msg := range tests {
// 		jsonString, jssErr := json.Marshal(msg)
// 		if jssErr != nil {
// 			t.Fatal(jssErr)
// 		}

// 		_, smErr := sqsClient.SendMessage(&sqs.SendMessageInput{
// 			MessageBody: aws.String(string(jsonString)),
// 			QueueUrl:    aws.String("http://localhost.localstack.cloud:4566/000000000000/server-logs"),
// 		})
// 		if smErr != nil {
// 			t.Fatalf("Failed to send message to SQS: %s", smErr)
// 		}
// 	}

// 	t.ResetTimer()
// 	t.Logf("Start Time: %d\n\n", time.Now().Unix())

// 	var done chan bool = make(chan bool)
// 	go func() {
// 		time.Sleep(20 * time.Second)
// 		done <- true
// 	}()

// 	mp.pollQueue(ctx, "http://localhost.localstack.cloud:4566/000000000000/server-logs", done) // Blocks
// }

func BenchmarkAddServersToQueue(t *testing.B) {
	sess := amazon.NewSession("us-east-1", "http://localhost.localstack.cloud:4566", 0, true)
	sqsClient := amazon.NewSQSClient(sess)

	f, fErr := os.Open("./testdata/20-test-servers.json")
	if fErr != nil {
		t.Fatal(fErr)
	}

	var tests []TestMessage
	byteValue, _ := io.ReadAll(f)
	jsErr := json.Unmarshal(byteValue, &tests)
	if jsErr != nil {
		t.Fatal(jsErr)
	}

	t.Logf("Total Items To Queue: %d\n", len(tests))

	for i, msg := range tests {
		t.Logf("Queueing Item #%d\n", i)
		jsonString, jssErr := json.Marshal(msg)
		if jssErr != nil {
			t.Fatal(jssErr)
		}

		_, smErr := sqsClient.SendMessage(&sqs.SendMessageInput{
			MessageBody: aws.String(string(jsonString)),
			QueueUrl:    aws.String("http://localhost.localstack.cloud:4566/000000000000/server-logs"),
		})
		if smErr != nil {
			t.Fatalf("Failed to send message to SQS: %s", smErr)
		}
	}
}
