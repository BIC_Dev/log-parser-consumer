package amazon

import (
	"io"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func UploadJSONToS3(client *s3.S3, jsonData io.Reader, bucket string, key string) error {
	uploader := s3manager.NewUploaderWithClient(client)
	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   jsonData,
	})

	if err != nil {
		return err
	}

	return nil
}
