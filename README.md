# Log Parser Consumer

## S3 Commands (localstack/awscli)
[Offical Docs](https://docs.localstack.cloud/aws/s3/)

### Create Bucket
```sh
awslocal s3api create-bucket --bucket server-logs
```

### List Buckets
```sh
awslocal s3api list-buckets
```

### List Objects
```sh
awslocal s3api list-objects --bucket server-logs
```

### List Objects in Bucket Summarized
```sh
aws --endpoint-url=http://localhost:4566 s3 ls --summarize --human-readable --recursive  s3://server-logs/
```

### Remove All Objects from Bucket
```sh
aws --endpoint-url=http://localhost.localstack.cloud:4566 s3 rm s3://server-logs --recursive
```